
// *****************************************************************************
// *****************************************************************************
//
//  ProtoC ProbeCard Firmware
//
//  This software is delivered "as is"...
//  Any past, present or future bugs are the responsibility of the user :-)
//
// *****************************************************************************
// *****************************************************************************

#include <xc.h>
#include <string.h>
#include <ctype.h>


#include <pic18f27j13.h>
//#include <plib/usart.h>


// *****************************************************************************
// *****************************************************************************
// Section: Configuration bits
// Internal RC Oscillator
// WDT OFF
// *****************************************************************************
// *****************************************************************************


//#pragma config OSC = INTOSC
//#pragma config WDTEN = OFF

// Setup for 48MHz Internal OSC
#pragma config WDTEN = OFF              // Sets watchdog OFF
#pragma config PLLDIV = 2               // Sets divider to 2. Provides output 4Mhz for 96Mhz PLL
#pragma config OSC = INTOSCPLL          // Sets OSC as INTOSC(8Mhz) Postscalled with a PLL
#pragma config CFGPLLEN = ON            // Enables PLL on startup
#pragma config PLLSEL = PLL96           // Selects 96Mhz

#pragma config XINST = OFF              // Disable Extended Instructions

#pragma config ADCSEL = BIT12           // Enable 12-bit A/D


// *****************************************************************************
// *****************************************************************************
// Section: System Macros
// *****************************************************************************
// *****************************************************************************

unsigned char CheckStatus();
unsigned char WriteReg(unsigned int,unsigned int);
#define _XTAL_FREQ 48000000



#define RES     LATCbits.LATC0
#define CS      LATCbits.LATC2
#define SCLK    LATCbits.LATC3
#define SDO     LATCbits.LATC5

#define SDI     PORTCbits.RC4

#define TEST    LATBbits.LATB3
#define LED2    LATBbits.LATB1
#define LED1    LATBbits.LATB0

#define SERIAL_ADDR  0x005000
#define CODEVER_ADDR 0x005500
#define DEBUG_ADDR   0x005A00


#define cr 13
#define lf 10

// -----------------------------------------------------------------------------
//
// RAM Definitions
//
//------------------------------------------------------------------------------

//
//	COM0 (RS232 port)
//	RX & TX Ring Definitions
//
#define tx0_buf_siz 64
#define rx0_buf_siz 124


unsigned char	rx0_buffer[rx0_buf_siz];                                        // COM0 RX Buffer
//unsigned char	rx0_bufend[2];                                                  // End of RX buffer

unsigned char	tx0_buffer[tx0_buf_siz];                                        // COM0 TX Buffer
//unsigned char	tx0_bufend[2];                                                  // End of TX buffer

unsigned int 	rx0_bytcnt;                                                     // COM0 RX Byte Count
unsigned int 	tx0_bytcnt;                                                     // COM0 TX Byte Count

char            *rx0_rd_adr;                                                    // COM0 RX Read adr
char            *rx0_wr_adr;                                                    // COM0 RX Write adr

char            *tx0_rd_adr;                                                    // COM0 TX Read adr
char            *tx0_wr_adr;                                                    // COM0 TX Write adr

char            tx0_data;                                                       // COM0 TX data */
char            COM0_data;                                                      // COM0 RX Read Ring data
char            COM0_rx_data;                                                   // COM0 RX Interrupt data


unsigned char	CMD_buffer[rx0_buf_siz];                                        // CMD Buffer
unsigned int    cmdbuf_ptr;                                                     // CMD Buffer index pointer
unsigned int    index;                                                          // CMD Buffer index pointer

unsigned char   nBits;                                                          // Number of SPI bits to send
unsigned int    TX_DATA;                                                        // SPI TX Data
unsigned int    RX_DATA;                                                        // SPI RX Data
char            rx_bit_cntr;                                                    // RX_DATA bit counter

char            ToggleIt;                                                       // LED toggle shadow register
char            LED_Flash_Timer;                                                // LED flash rate timer

char            Read_Status;                                                    // Read ProtoC status byte
unsigned int    Read_Address;                                                   // 16-bit HEX address
unsigned int    Read_Data;                                                      // 16-bit HEX data

unsigned short long FWR_Address;                                                // Flash address

unsigned int    WR_Address;                                                     // Write CMD address
unsigned int    WR_Data;                                                        // Write CMD data
unsigned int    REG0_value;
unsigned int    REG1_value;
unsigned int    REG2_value;                                                     // Register 2 value for TUNE cmd
unsigned int    REG3_value;                                                     // Register 3 value for TUNE cmd
unsigned int    Commanded_value;                                                // Commanded value for WriteReg cmd
unsigned int    Returned_value;                                                 // Returned value from ReadReg cmd
unsigned int    OTP_data,icnt;                                                  // OTP data value
unsigned int    TEMP_data;                                                      // Temp data
unsigned int    ADC_raw;                                                        // ADC last-read data
unsigned int    MUX_chnl;                                                       // ADC MUX channel
unsigned int    ADC_adcon0;                                                     // ADCON0 shadow register
unsigned int    ADC_data[5];                                                    // ADC Conversion Array
unsigned int    AN0_data;                                                       // AN0 data
unsigned int    AN1_data;                                                       // AN1 data
unsigned int    AN2_data;                                                       // AN2 data
unsigned int    AN3_data;                                                       // AN3 data
unsigned int    AN4_data;                                                       // AN4 data
unsigned int    AN8_data;                                                       // AN8 data

unsigned int    GEN_REG;                                                        //H/I specific function register

unsigned int    lcv;                                                            //loop control variable

unsigned char   Error_flag;                                                     // Error flag
unsigned char   CheckStatusError_flag;                                          //Check status error flag
unsigned char   CheckStatus_flag;                                               // Check Status flag

// *****************************************************************************
//
// High Priority Interrupt Service Routine
//
// *****************************************************************************

void interrupt low_priority Low_ISR()

{
    //PORTC = PORTC | 0x01;
}




// *****************************************************************************
//
// High Priority Interrupt Service Routine
//
// *****************************************************************************
//void interrupt high_priority High_ISR()
void interrupt High_ISR()
{    
    unsigned char dummy;
  //
  // RX Interrupts
  //
        if (RCSTAbits.OERR)                                                     // Over-Run bit set?
	   {
	    (RCSTAbits.CREN = 0);                                                   // Reset CREN bit to clear the over-run error
	    (RCSTAbits.CREN = 1);                                                   // Enable CREN bit to enable the UART
	   }

  if (PIR1bits.RCIF)                                                            // RX Interrupt Flag set?
    {
        if (rx0_bytcnt >= rx0_buf_siz)                                          // Is RX buffer full?
        {
            dummy = RCREG;                                                      // Dummy read to clear out RX register and RCIF bit
            return;                                                             // Return without storing in ring
        }

        rx0_bytcnt++;                                                           // Increment RX Byte Count
        *rx0_wr_adr = RCREG;                                                    // Store rx data in ring
        rx0_wr_adr++;                                                           // Increment RX write adr
        if (rx0_wr_adr == &rx0_buffer[0] + rx0_buf_siz)                         // Is Write pointer at end of ring?
            rx0_wr_adr = &rx0_buffer[0];                                        // Reset pointer to beginning of ring

       // mPORTEToggleBits(LED1);                                               // Toggle LED

    } // RX Interrupt

  
  //
  // TX Interrupts
  //
  if (PIR1bits.TXIF)
    {
        if (tx0_bytcnt == 0)                                                    // Is TX Byte Count zero?
          {
            PIE1bits.TXIE = 0;                                                  // Disable TX Interrupts
             return;                                                            // Return if nothing else to TX
          }


        tx0_bytcnt--;                                                           // Dec TX Byte Count
//      if (tx0_bytcnt == 0)                                                    // Is TX Byte Count zero?
//        {
//          PIE1bits.TXIE = 0;                                                  // Disable TX Interrupts
//        }

        tx0_data = *tx0_rd_adr;                                                 // Get TX Character from ring
        TXREG = tx0_data;                                                       // Output TX Character
        tx0_rd_adr++;                                                           // Inc TX Read adr
        if (tx0_rd_adr == &tx0_buffer[0] + tx0_buf_siz)                         // Is Read pointer at end of ring?
            tx0_rd_adr = &tx0_buffer[0];                                        // Reset pointer to beginning of ring

        //INTClearFlag(INT_SOURCE_UART_TX(UART3));                              // Clear TX Interrupt flag

    } // TX Interrupt

} // Interrupt Handler


// =============================================================================
//
//  Initialize COM0 RX & TX
//
// =============================================================================

void Initialize_COM0_RX( void )
{
    rx0_rd_adr = &rx0_buffer[0];                                                // Initialize RX read pointer
    rx0_wr_adr = &rx0_buffer[0];                                                // Initialize RX write pointer
    rx0_bytcnt = 0;                                                             // Reset RX byte count
}

void Initialize_COM0_TX( void )
{
    tx0_rd_adr = &tx0_buffer[0];                                                // Initialize TX read pointer
    tx0_wr_adr = &tx0_buffer[0];                                                // Initialize TX write pointer
    tx0_bytcnt = 0;                                                             // Reset TX byte count
}


// =============================================================================
//
//  Read COM0 RX Ring
//  Function returns false if ring is empty
//  If ring is not empty, COM0_data = data from ring
//
// =============================================================================
unsigned char Read_COM0 ( void )
{
    if (rx0_bytcnt == 0)                                                        // Any data in ring?
        return(0);                                                              // Return with no data available

    //Disable RX Interrupts while reading from ring
    PIE1bits.RCIE = 0;                                                          // Disable RX Interrupts

    COM0_data = *rx0_rd_adr;                                                    // Read data from ring
    rx0_rd_adr++;                                                               // Inc read pointer

    if (rx0_rd_adr == &rx0_buffer[0] + rx0_buf_siz)                             // Is pointer at end of ring?
        rx0_rd_adr = &rx0_buffer[0];                                            // Reset pointer to beginning of ring

    rx0_bytcnt--;                                                               // Decrement rx byte count

    // Enable RX Interrupts
    PIE1bits.RCIE = 1;                                                          // Enable RX Interrupts
    
    return(1);                                                                  // Return with data available

} /* Read_COM0 */



// =============================================================================
//
//  Store character in COM0 TX Ring
//
//  TX Interrupts are enabled if TX has not been started
//
// =============================================================================
unsigned char Write_COM0 (unsigned char tx0data)
{
    if (tx0_bytcnt >= tx0_buf_siz)                                              // Is TX Ring full?
        return(0);                                                              // Return FALSE if full
    
    // Disable TX Interrupts while updating TX ring
    PIE1bits.TXIE = 0;                                                          // Disable TX Interrupts

    tx0_bytcnt++;                                                               // Increment TX Byte Count
    *tx0_wr_adr = tx0data;                                                      // Read TX Data and store in rx ring
    tx0_wr_adr++;                                                               // Increment TX write adr
    if (tx0_wr_adr == &tx0_buffer[0] + tx0_buf_siz)
        tx0_wr_adr = &tx0_buffer[0];                                            // Reset pointer to beginning of ring

    // Enable TX Interrupts
    PIE1bits.TXIE = 1;                                                          // Enable TX Interrupts
    return(1);                                                                  // Return TRUE

} /* Write_COM0 */




// =============================================================================
//
//  Get Command
//
//  Wait for command string (terminated by cr)
//
// =============================================================================
unsigned char Get_CMD_String ( void )
{
    
    if (Read_COM0())                                                            // Rcv'd character?
    {
        CMD_buffer[index] = COM0_data;                                          // Store characters in CMD buffer
        Write_COM0(COM0_data);                                                  // Echo character

        if (COM0_data == cr)                                                    // "cr" entered?
        {
            CMD_buffer[index] = 0;                                              // Terminate string with null
            return(1);
        }
    index++;                                                                    // Inc index for next character
    } // if (Read_COM0)

    return(0);

} // Get_CMD_String

// =============================================================================
// =                                                                           =
// =	Configure Stuart                                                       =
// =    This routine sends LSB data first                                      =
// =                                                                           =
// =============================================================================

void Config()
{
       //CheckStatus();
       RES = 1;                                                                 // Set RESET line high
       __delay_ms(1);
       RES = 0;                                                                 // Return RESET line to low
       WriteReg(0x001d,1);
       WriteReg(0x0024,1);
       WriteReg(0x0025,1);
       WriteReg(0x0026,1);
       WriteReg(0x0027,0);
       WriteReg(0x0004,0);
       WriteReg(0x001e,7);
       WriteReg(0x001f,3);
       return;
} // Config


// -----------------------------------------------------------------------------
//
// Delay
//
//------------------------------------------------------------------------------
void hang_out()
{
    long int i;
    long int counter;

    counter = 0;
    for (i=0; i<100000; i++)
    {
        counter++;
    }

} //hang_out


// -----------------------------------------------------------------------------
//
// LED Delay
//
//------------------------------------------------------------------------------
void LED_delay()
{
    int i;

    for (i=0; i<30; i++)
    {
       __delay_ms(2);
    }

} //LED_delay


// -----------------------------------------------------------------------------
//
// ACK LED
//
//------------------------------------------------------------------------------
void ACK_LED()
{
    LED2 = 1;
    LED_delay();
    LED2 = 0;

} //ACK LED


// -----------------------------------------------------------------------------
//
// Read ADC Channels AN0 ~ AN4
//
//------------------------------------------------------------------------------
void Read_AN0_AN4()
{
    // Read AN0
    ADCON0 = 0x03;                                                              // Set MUX / Start conversion
    __delay_ms(2);                                                              // Wait til done
    ADC_raw = ADRESH << 8;                                                      // Read MSB data and shift to upper byte
    ADC_raw = ADC_raw | ADRESL;                                                 // OR'in LSB data
    AN0_data = ADC_raw;                                                         // Store conversion
    
    // Read AN1
    ADCON0 = 0x07;                                                              // Set MUX / Start conversion
    __delay_ms(2);                                                              // Wait til done
    ADC_raw = ADRESH << 8;                                                      // Read MSB data and shift to upper byte
    ADC_raw = ADC_raw | ADRESL;                                                 // OR'in LSB data
    AN1_data = ADC_raw;                                                         // Store conversion
    
    // Read AN2
    ADCON0 = 0x0B;                                                              // Set MUX / Start conversion
    __delay_ms(2);                                                              // Wait til done
    ADC_raw = ADRESH << 8;                                                      // Read MSB data and shift to upper byte
    ADC_raw = ADC_raw | ADRESL;                                                 // OR'in LSB data
    AN2_data = ADC_raw;                                                         // Store conversion
    
    // Read AN3
    ADCON0 = 0x0F;                                                              // Set MUX / Start conversion
    __delay_ms(2);                                                              // Wait til done
    ADC_raw = ADRESH << 8;                                                      // Read MSB data and shift to upper byte
    ADC_raw = ADC_raw | ADRESL;                                                 // OR'in LSB data
    AN3_data = ADC_raw;                                                         // Store conversion
    
    // Read AN4
    ADCON0 = 0x13;                                                              // Set MUX / Start conversion
    __delay_ms(2);                                                              // Wait til done
    ADC_raw = ADRESH << 8;                                                      // Read MSB data and shift to upper byte
    ADC_raw = ADC_raw | ADRESL;                                                 // OR'in LSB data
    AN4_data = ADC_raw;                                                         // Store conversion
    
    
} // Read_AN0_AN4


// -----------------------------------------------------------------------------
//
// Read ADC Channels AN8 - BATT_MON
//
//------------------------------------------------------------------------------
void Read_AN8()
{
    // Read AN8
    ADCON0 = 0x23;                                                              // Set MUX / Start conversion
    __delay_ms(2);                                                              // Wait til done
    ADC_raw = ADRESH << 8;                                                      // Read MSB data and shift to upper byte
    ADC_raw = ADC_raw | ADRESL;                                                 // OR'in LSB data
    AN8_data = ADC_raw;                                                         // Store conversion

    
} // Read_AN8



// -----------------------------------------------------------------------------
//
// Check Nominal Voltage
//
//------------------------------------------------------------------------------
unsigned int Check_Limits(unsigned int ADC_value, unsigned int NOM_voltage, unsigned int tolerance)
{   
    unsigned int Lo_limit, Hi_limit;
    
    Lo_limit = NOM_voltage - tolerance;                                         // Calculate low limit value
    Hi_limit = NOM_voltage + tolerance;                                         // Calculate high limit value
    
    //if (ADC_value < Lo_limit) { return(1); }                                    // ADC value is < low limit
    
    if (ADC_value > Hi_limit) { return(2); }                                    // ADC value is > high limit
    
    return(0);                                                                  // Everything is OK :-)
            
    
} // Check_Limits



// -----------------------------------------------------------------------------
//
// WriteString
//
// Write string to tx0_buffer and start transmitting characters
//
//------------------------------------------------------------------------------
void WriteString(const char *string)
{
  while (*string != '\0')
    {
     Write_COM0(*string++);                                                     // Output string to tx0 buffer

      //while (!TXSTAbits.TRMT);                                                // Wait until TX buffer is empty
      //TXREG = *string;
      //string++;
      //while (!TXSTAbits.TRMT);                                                // Wait until TX buffer is empty
    }
} // WriteString


// -----------------------------------------------------------------------------
//
// Display HEX Nibble
//
//------------------------------------------------------------------------------
void Display_Nibble(unsigned char nibble)
{
    nibble = nibble & 0x0F;                                                     // Mask upper bits
    
    if (nibble < 10)                                                            // 0 - 9?
    {
        nibble = nibble | 0x30;                                                 // Add ASCII
        Write_COM0(nibble);                                                     // Output "0 - 9" character
        return;
    }
    
    // nibble is A - F
    nibble = nibble + 55;                                                       // Add ASCII offset
    Write_COM0(nibble);                                                         // Output "A - F" character
    
} // Display_Nibble



// -----------------------------------------------------------------------------
//
// Display HEX Value
//
//------------------------------------------------------------------------------
void Display_HEX16(unsigned int hex16)
{
    unsigned char nibble;
    
    WriteString("0x");                                                          // Output HEX prefix
    
    nibble = hex16 >> 12;                                                       // Shift 1st nibble to LSB
    Display_Nibble(nibble);                                                     // Output character
    
    nibble = hex16 >> 8;                                                        // Shift 2nd nibble to LSB
    Display_Nibble(nibble);                                                     // Output character
    
    nibble = hex16 >> 4;                                                        // Shift 3rd nibble to LSB
    Display_Nibble(nibble);                                                     // Output character
    
    nibble = hex16;                                                             // Get 4th nibble
    Display_Nibble(nibble);                                                     // Output character
    
    
} // Display_HEX16

void Display_HEX24(unsigned short long hex24)
{
    unsigned char nibble;
    
    WriteString("0x");                                                          // Output HEX prefix
    
    nibble = hex24 >> 20;                                                       // Shift 1st nibble to LSB
    Display_Nibble(nibble);                                                     // Output character
    
    nibble = hex24 >> 16;                                                        // Shift 2nd nibble to LSB
    Display_Nibble(nibble);                                                     // Output character
    
    
    nibble = hex24 >> 12;                                                       // Shift 3rd nibble to LSB
    Display_Nibble(nibble);                                                     // Output character
    
    nibble = hex24 >> 8;                                                        // Shift 4th nibble to LSB
    Display_Nibble(nibble);                                                     // Output character
    
    nibble = hex24 >> 4;                                                        // Shift 5th nibble to LSB
    Display_Nibble(nibble);                                                     // Output character
    
    nibble = hex24;                                                             // Get 6th nibble
    Display_Nibble(nibble);                                                     // Output character
    
    
} // Display_HEX24


// -----------------------------------------------------------------------------
//
// Display Decimal Value
//
//------------------------------------------------------------------------------
void Display_DEC(unsigned int hex16)
{
    unsigned int digit, keep_printing;
    
    keep_printing = 0;                                                          // Clear keep-printing flag
    
    digit = hex16 / 10000;                                                      // Find 10000 digit
    if (digit > 0)
    {
        Write_COM0(digit | 0x30);                                               // Add ASCII & Display digit
        hex16 = hex16 - (digit * 10000);                                        // Subtract 10000 digit
        keep_printing = 1;                                                      // Set keep_printing flag
    }
    
    digit = hex16 / 1000;                                                       // Find 1000 digit
    if (digit > 0 | keep_printing)                                              // Display digit it if is non-zero or keep_printing flag is set
    {
        Write_COM0(digit | 0x30);                                               // Add ASCII & Display digit
        hex16 = hex16 - (digit * 1000);                                         // Subtract 1000 digit
        keep_printing = 1;                                                      // Set keep_printing flag
    }
    
    digit = hex16 / 100;                                                        // Find 100 digit
    if (digit > 0 | keep_printing)                                              // Display digit it if is non-zero or keep_printing flag is set
    {
        Write_COM0(digit | 0x30);                                               // Add ASCII & Display digit
        hex16 = hex16 - (digit * 100);                                          // Subtract 100 digit
        keep_printing = 1;                                                      // Set keep_printing flag
    }
    
    digit = hex16 / 10;                                                         // Find 10 digit
    if (digit > 0 | keep_printing)                                              // Display digit it if is non-zero or keep_printing flag is set
    {
        Write_COM0(digit | 0x30);                                               // Add ASCII & Display digit
        hex16 = hex16 - (digit * 10);                                           // Subtract 10 digit
    }
    
    // Alwasy print last digit
    digit = hex16;                                                              // Get remainging portion of number
    Write_COM0(digit | 0x30);                                                   // Add ASCII & Display digit
    
    
} // Display_DEC



// -----------------------------------------------------------------------------
//
// PutCharacter
//
//------------------------------------------------------------------------------
void PutCharacter(const char character)
{
  while (!TXSTAbits.TRMT);
  TXREG = character;
  while (!TXSTAbits.TRMT);
} // PutCharacter


// =============================================================================
//
//  Find ZERO or ONE from CMD Buffer string
//
// =============================================================================
char Find_ZERO_or_ONE (void)
{
    int i;
    for (i=0; i<8; i++)                                                         // Start looking for "1" or "0"
    {
        if (CMD_buffer[cmdbuf_ptr] == '1')                                      // Find "1"?
            return(1);                                                          // Return if yes

        if (CMD_buffer[cmdbuf_ptr] == '0')                                      // Find "0"
            return(0);                                                          // Return if yes

        cmdbuf_ptr++;                                                           // Inc pointer and keep looking
    }

    return(0);                                                                  // Default to "0"

} // Find_ZERO_or_ONE


// =============================================================================
//
//  Find decimal Number from CMD Buffer string
//  Return with decimal number
//
// =============================================================================
int Find_Number (void)
{
    int i;
    int digit;
    int number;

    for (i=0; i<8; i++)                                                         // Start looking for ASCII numbers
    {
        digit = CMD_buffer[cmdbuf_ptr];                                         // Get character from buffer
        if (isdigit(digit) )                                                    // Check to see if character is a digit
            goto found_digit;                                                   // Jump if found a digit

        cmdbuf_ptr++;                                                           // Increment buffer pointer
    } // for

    return(0);                                                                  // Unable to find a number...

found_digit:

    number = 0;                                                                 // Initialize number
    for (i=0; i<6; i++)                                                         // Convert digits to decimal number
    {
        digit = CMD_buffer[cmdbuf_ptr++];                                       // Get character from buffer
        if (isdigit(digit) == 0)                                                // Is character a valid number?
            return(number);                                                     // Return if not

        number = (number * 10) + (digit & 0x0F);                                // Convert to decimal
    } // for

    return(number);
} // Find_Number


// =============================================================================
//
//  Find 2-digit HEX Number from CMD Buffer string
//  Return with HEX number
//
// =============================================================================
int Find_HEX_Number (void)
{
    int i;
    int digit;
    int number;
    number = 0xFF;
    

    for (i=0; i<8; i++)                                                         // Start looking for ASCII HEX characters
    {
        digit = CMD_buffer[cmdbuf_ptr];                                         // Get character from buffer
        if (isxdigit(digit))                                                    // Check to see if character is a HEX character
            goto found_HEX_digit;                                               // Jump if found HEX digit

        cmdbuf_ptr++;                                                           // Increment buffer pointer
    } // for

    return(0);                                                                  // Unable to find a number...

found_HEX_digit:
    // Get Upper Nibble
    if (isdigit(digit))                                                         // Is character 0 ~ 9?
        digit = digit & 0x0F;                                                   // Mask ASCII
    else
        digit = digit - 0x37;                                                   // Convert A ~ F to 11 ~ 15

    number = digit;                                                             // Save HEX number

    cmdbuf_ptr++;                                                               // Inc to next character
    digit = CMD_buffer[cmdbuf_ptr];                                             // Get next character
    if (digit == ',')                                                           // End of HEX number?
        return(number);                                                         // Return if yes
    if (digit == cr)                                                            // End of HEX number?
        return(number);                                                         // Return if yes
    if (digit == lf)                                                            // End of HEX number?
        return(number);                                                         // Return if yes
    if (digit == 0)                                                             // End of HEX number?
        return(number);                                                         // Return if yes

    number = number << 4;                                                       // Store first Nibble and shift left

    // Get Lower Nibble
    //cmdbuf_ptr++;                                                             // Inc point to next character
    //digit = CMD_buffer[cmdbuf_ptr];                                           // Get character from buffer
    if (isdigit(digit))                                                         // Is character 0 ~ 9?
        digit = digit & 0x0F;                                                   // Mask ASCII
    else
        digit = digit - 0x37;                                                   // Convert A ~ F to 11 ~ 15

    number = number | digit;                                                    // Add lower nibble to number
    
    cmdbuf_ptr++;                                                               // Inc to next character

    return(number);                                                             // Return with result

} // Find_HEX

// =============================================================================
//
//  Find up to 4-digit HEX Number from CMD Buffer string
//  Return with HEX number
//
// =============================================================================
unsigned int Find_HEX16_Number (void)
{
    int i;
    unsigned int digit;
    unsigned int number;
    number = 0;
    

    for (i=0; i<8; i++)                                                         // Start looking for ASCII HEX characters
    {
        digit = CMD_buffer[cmdbuf_ptr];                                         // Get character from buffer
        if (isxdigit(digit))                                                    // Check to see if character is a HEX character
            goto found_HEX16_digit;                                             // Jump if found HEX digit

        cmdbuf_ptr++;                                                           // Increment buffer pointer
    } // for

    return(0);                                                                  // Unable to find a number...

found_HEX16_digit:
    // Convert ASCII characters to HEX and add to "number"

    for (i=0; i<4; i++)
    {
        if (isdigit(digit))                                                     // Is character 0 ~ 9?
            digit = digit & 0x0F;                                               // Mask ASCII
        else
            digit = toupper(digit) - 0x37;                                               // Convert A ~ F to 11 ~ 15

        number = number << 4;                                                   // Shift number left to add next digit
        number = number | digit;                                                // OR'in next digit

        cmdbuf_ptr++;                                                           // Inc to next character
        digit = CMD_buffer[cmdbuf_ptr];                                         // Get next character
        if (digit == ' ')                                                       // End of HEX number?
            return(number);                                                     // Return if yes
        if (digit == ',')                                                       // End of HEX number?
            return(number);                                                     // Return if yes
        if (digit == cr)                                                        // End of HEX number?
            return(number);                                                     // Return if yes
        if (digit == lf)                                                        // End of HEX number?
            return(number);                                                     // Return if yes
        if (digit == 0)                                                         // End of HEX number?
            return(number);                                                     // Return if yes
    
    } // for...

} // Find_HEX16_Number


// =============================================================================
//
//  Find up to 6-digit HEX Number from CMD Buffer string
//  Return with HEX number
//
// =============================================================================
unsigned short long Find_HEX24_Number (void)
{
    int i;
    unsigned int digit;
    unsigned short long number;
    number = 0;

    for (i=0; i<8; i++)                                                         // Start looking for ASCII HEX characters
    {
        digit = CMD_buffer[cmdbuf_ptr];                                         // Get character from buffer
        if (isxdigit(digit))                                                    // Check to see if character is a HEX character
            goto found_HEX24_digit;                                             // Jump if found HEX digit

        cmdbuf_ptr++;                                                           // Increment buffer pointer
    } // for

    return(0);                                                                  // Unable to find a number...

found_HEX24_digit:
    // Convert ASCII characters to HEX and add to "number"

    for (i=0; i<6; i++)
    {
        if (isdigit(digit))                                                     // Is character 0 ~ 9?
            digit = digit & 0x0F;                                               // Mask ASCII
        else
            digit = digit - 0x37;                                               // Convert A ~ F to 11 ~ 15

        number = number << 4;                                                   // Shift number left to add next digit
        number = number | digit;                                                // OR'in next digit

        cmdbuf_ptr++;                                                           // Inc to next character
        digit = CMD_buffer[cmdbuf_ptr];                                         // Get next character
        if (digit == ' ')                                                       // End of HEX number?
            return(number);                                                     // Return if yes
        if (digit == ',')                                                       // End of HEX number?
            return(number);                                                     // Return if yes
        if (digit == cr)                                                        // End of HEX number?
            return(number);                                                     // Return if yes
        if (digit == lf)                                                        // End of HEX number?
            return(number);                                                     // Return if yes
        if (digit == 0)                                                         // End of HEX number?
            return(number);                                                     // Return if yes
    
    } // for...

} // Find_HEX24_Number


// -----------------------------------------------------------------------------
//
// CheckStatus
//
// implement check status command (0x03)
//
//------------------------------------------------------------------------------
unsigned char CheckStatus()
{
    unsigned int i=1;
    CS = 0;          //start of SPI write/read
    // Transmit 0x3 for check status
    SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SDO= 0;    //check status command
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;
    SDO = 1;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;SCLK = 0;SCLK = 0;SCLK = 0;
    SCLK = 0;SCLK = 0;SCLK = 0;SCLK = 0;
    SDO = 0;
    

//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
//    SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1;SCLK=1; SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    
    
    
    
    SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    CS=1;
    return SDI;
} //CheckStatus


// -----------------------------------------------------------------------------
//
// WriteReg
//
// implement write register command (0x00)
//
//------------------------------------------------------------------------------
unsigned char WriteReg(unsigned int r,unsigned int d){
    unsigned int i,c;
    c = r;
    CS = 0;          //start of SPI write/read
    // Transmit 0x0 for write
    SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SDO= 0;   
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    for (i=0;i<16;i++){
        if ((c & 0x8000)!= 0){SDO=1;}
        else {SDO=0;}
        SCLK=1;SCLK=1;SCLK = 0;SCLK=0;
        c<<=1;
    }
    c = d;
    SDO= 0;
    for (i=0;i<16;i++){
        if ((c & 0x8000)!=0){ SDO=1;}
        else {SDO=0;}
        if (i != 15) {SCLK=1;SCLK=1;SCLK = 0;SCLK=0;}
        else {SCLK=1;SCLK=1;SCLK=0;SDO=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SDO=0;
        CS=1;}
        c<<=1;
    }
    CheckStatus_flag = 0;
    icnt = 0;
    while(icnt<7000 && !CheckStatus_flag){CheckStatus_flag = CheckStatus();icnt+=1;}   // was icnt<7000
    return(CheckStatus_flag);
} //WriteReg

//-----------------------------------------------------------------------------
//
// ReadReg
//
// implement read command (0x01 and 0x02)
//
//------------------------------------------------------------------------------
unsigned int ReadReg(unsigned int r)
{
    unsigned char i,msb;
    unsigned int c;
    c = r;
    CS = 0;          //start of SPI write/read
    // Transmit 0x1 for start read
    SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SDO= 0;    //check status command
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;
    SDO = 1;
    SCLK=1;SCLK=1;SCLK = 0;
    for (i=0;i<15;i++){
        if ((c & 0x8000)!=0)SDO=1;
        else SDO=0;
        SCLK=1;c<<=1;SCLK = 0;
    }
    if (c & 0x8000)SDO=1;
    else SDO=0;
    SCLK=1;SCLK=1;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SCLK=0;SDO=0;
    CS=1;
    CheckStatus_flag = 0;
    i = 0;
    while(i<100 & !CheckStatus_flag){CheckStatus_flag = CheckStatus();i+=1;}
    //if(!CheckStatus_flag) { return 0xFFFF; }
    CS=0;
    RX_DATA = 0;
    SDO=0;
    //Send 2, and then read in next 16 bits
    SCLK=0;SCLK=0;SCLK=0;SCLK=0;    //02, end read command
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;SCLK=1;SCLK = 0;
    SDO = 1;
    SCLK=1;SCLK=1;SCLK = 0;
    SDO=0;
    //SCLK=1;SCLK=1;SCLK = 0;SCLK = 0;
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<= 1;                                                    // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;
    RX_DATA = RX_DATA <<=1;                                                     // Shift RX data right 1 bit
    SCLK=1;
    if (SDI == 1) RX_DATA = RX_DATA ^ 0x0001; // OR in one if not zero
    SCLK=0;SCLK=0;SCLK=0;SCLK=0;
    CS=1;
    CheckStatus_flag = 0;
    i = 0;
    while(i<100 & !CheckStatus_flag){CheckStatus_flag = CheckStatus();i+=1;}    // was i<100
    if(!CheckStatus_flag) { return 0xFFFF; }
    return (RX_DATA);
} //ReadReg */

// -----------------------------------------------------------------------------
//
// WriteFlash
//
// implement write flash
//
//------------------------------------------------------------------------------
void WriteFlash(unsigned short long addr, unsigned int val){
    //Load TBLPTR with the base address
    TBLPTR = addr;
       
    //LSB of word to be written
    TABLAT = (val >> 8);
    asm("TBLWT*+");
    //MSB of word to be written
    TABLAT = (val >> 16);
    asm("TBLWT*");
       
    //Program Memory write
    EECON1bits.WPROG = 1; //enable single word write
    EECON1bits.WREN = 1; //enable write to memory
    INTCONbits.GIE = 0; //disable interrupts
     
    //unlock flash memory
    EECON2 = 0x55; //write 55h
    EECON2 = 0xAA; //write AAh
     
    EECON1bits.WR = 1; //start program :CPU stall
    while(EECON1bits.WR) {continue;} //above code should auto-stall but just making sure
       
       
    EECON1bits.WPROG = 0; //disable single word write
    EECON1bits.WREN = 0; //disable write to memory
    INTCONbits.GIE = 1; //enable interrupts
       
    return;
}

//-----------------------------------------------------------------------------
//
// ReadFlash
//
// implement read from flash memory
//
//------------------------------------------------------------------------------
unsigned int ReadFlash(unsigned short long addr) {
    //Load TBLPTR with the base address
    TBLPTR = addr;
       
    //READ_WORD
    asm("TBLRD*+");  
    char WORD_EVEN = TABLAT;
    asm("TBLRD*+");
    char WORD_ODD = TABLAT;
    return ((WORD_EVEN << 8) | WORD_ODD);
}

//------------------------------------------------------------------------------
//
// EraseFlash
//
//implement a flash erase so the memory can be rewritten
//
//------------------------------------------------------------------------------
void FlashErase(unsigned short long addr) {
    TBLPTR = addr;
    
    EECON1bits.WREN = 1;
    EECON1bits.FREE = 1;
    INTCONbits.GIE = 0;
    
    //unlock flash memory
    EECON2 = 0x55; //write 55h
    EECON2 = 0xAA; //write AAh
     
    EECON1bits.WR = 1; //start program :CPU stall
    while(EECON1bits.WR) {continue;} //above code should auto-stall but just making sure
       
    INTCONbits.GIE = 1;
    EECON1bits.WREN = 0;
    EECON1bits.FREE = 0;   
}


// -----------------------------------------------------------------------------
//
// Command functions
//
// All complex commands have their own functions to allow easier command editing
// returns -1 if failed or 0 for success
//
// All parameters are set in globals within the main function
//
// -----------------------------------------------------------------------------

/*******************************************************************************
 *
 * Reads the chip ID from OTP
 *  
 ******************************************************************************/
int IDRD() {
    Returned_value = ReadReg(0x0044);                           
    if(Returned_value == 0x1250) {                                              //Determine if the chip is I generation or H generation
        OTP_data = 0;                                                           // Initialize OTP data value
        int i;
        for(i=0; i<20; i++) {                                                   // Read OTP multiple times to check for 1's
            OTP_data = OTP_data | ReadReg(0x0039);                              // Read OTP value and 'OR' in previous value

            if (CheckStatus_flag == 0) {
                break;
            }
        } // for...


        WriteString("\r\n");
        Display_HEX16(OTP_data);                                                // Display chip ID
       __delay_ms(2); 
        if (CheckStatus_flag == 1) {
            WriteString("\r\nOK\r\n");
            ACK_LED();
        } else {
          WriteString("\r\nERR\r\n");  
        }
        __delay_ms(2);
    } else {
        OTP_data = 0;                                                           // Initialize OTP data value
        int i;
        for(i=0; i<20; i++) {                                                   // Read OTP multiple times to check for 1's
            OTP_data = OTP_data | ReadReg(0x0038);                              // Read OTP value and 'OR' in previous value

            if (CheckStatus_flag == 0) {
                break;
            }
        } // for...


        WriteString("\r\n");
        Display_HEX16(OTP_data);                                                // Display chip ID
       __delay_ms(2); 

        if (CheckStatus_flag == 1) {
            WriteString("\r\nOK\r\n");
            ACK_LED();
        } else {
          WriteString("\r\nERR\r\n");  
        }
        __delay_ms(2);
    }
}

/*******************************************************************************
 *
 * Clears all the OTP backed registers to save a chip ID
 * If keepSet == 1, registers 0-3 are kept the same
 * 
 ******************************************************************************/
int IDWR(int keepSet) {
    unsigned int lcv;                                                           //a loop control variable
    Returned_value = ReadReg(0x0044);                           
    if(Returned_value == 0x1250) {                                              //Determine if the chip is I generation or H generation
        if(keepSet == 0) {
            cmdbuf_ptr = 5;                                                     // Set pointer to end of key word
            WR_Data = Find_HEX16_Number();                                      // Save Chip ID to write
            RES = 1;
            __delay_us(100);
            RES = 0;
            __delay_ms(10);
            
            WriteReg(0x0,0);
            WriteReg(0x1,0);
            WriteReg(0x2,0);                                                    // clear course tune register
            WriteReg(0x3,0);                                                    // clear fine tune register
            
            WriteReg(0x39,WR_Data);
        }
        
        for(lcv=0x04;lcv<=0x1B;lcv++) {
            WriteReg(lcv,0);
            __delay_ms(1);
        }
        
        WriteReg(0x24,0);
        WriteReg(0x2A,0);
        WriteReg(0x31,0);
        for(lcv=0x3F;lcv<=0x43;lcv++) {
            WriteReg(lcv,0);
        }
        __delay_ms(5);
        
        WriteReg(0x57,0);
        WriteReg(0x58,0);
        WriteReg(0x61,0);
        for(lcv=0x67;lcv<=0x6C;lcv++) {
            WriteReg(lcv,0);
        }
        
        __delay_ms(5);
        
        //Power OTP module
        WriteReg(0x27,1);
        WriteReg(0x65,8);
        WriteReg(0x66,8);
        WriteReg(0x28,0);
        
        WriteReg(0x4D,1);                                                       //Autoload OTP contents on reset
       
        WriteReg(0x75,0xFFFF);                                                  //Enable OTP write
                
        
    } else {
        if(keepSet == 0) {
            cmdbuf_ptr = 5;                                                     // Set pointer to end of key word
            WR_Data = Find_HEX16_Number();                                      // Save Chip ID to write

            RES = 1;
            __delay_us(100);
            RES = 0;
            __delay_us(100);

            WriteReg(0x0,0);
            WriteReg(0x1,0);
            WriteReg(0x2,0);                                                    // clear course tune register
            WriteReg(0x3,0);                                                    // clear fine tune register
            WriteReg(0x40,0xC);                                                 // Default value?
            WriteReg(0x38,WR_Data);
            //CheckStatus_flag = CheckStatus();
            WriteReg(0x004C,0xFFFF);                                            // Enable OTP Write
        }
        for(lcv=0x4;lcv<0x1B;lcv++) {
            WriteReg(lcv,0);
        }
        WriteReg(0x26,0);
        WriteReg(0x2D,0);
        WriteReg(0x44,0x0000);
        //CheckStatus_flag = CheckStatus();
        WriteReg(0x004C,0xFFFF);                                                // Enable OTP Write
        
    }
    if (CheckStatus_flag == 1) {
        WriteString("\r\nOK\r\n");
        ACK_LED();
    } else {
        WriteString("\r\nERR: CheckStatus_flag issue\r\n");  
    }
    __delay_ms(2);
}

/*******************************************************************************
 *
 * Sets the registers for continues wave output and uses user values for 0x2,0x3
 * 
 ******************************************************************************/
int TUNE(int CW) {
    
    cmdbuf_ptr = 5;                                                             // Set pointer to end of key word
    REG2_value = Find_HEX16_Number();                                           // Get register 2 value
    REG3_value = Find_HEX16_Number();                                           // Get register 3 value

    Error_flag = 0;                                                             // Initialize Error_flag

    
    CheckStatus_flag = 1;
    icnt = 0;
    while(icnt<4000 && !CheckStatus_flag){CheckStatus_flag = CheckStatus();icnt+=1;}
    //WriteReg(0x1D, 1);
    //Returned_value = ReadReg(0x1D);
    //Returned_value = ReadReg(0x1D);
    //Returned_value = ReadReg(0x1D);

    Returned_value = ReadReg(0x0044);                           
    if(Returned_value == 0x1250) {                                              //Determine if the chip is I generation or H generation
        //WriteString("\r\nI Generation CHIP");        
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x005A,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x005A);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }
        
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x001E,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x001E);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
        }

        Commanded_value = 7;                                                    // Value sent to register                   
        WriteReg(0x001F,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x001f);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
        }

        Commanded_value = 3;                                                    // Value sent to register                   
        WriteReg(0x0020,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0020);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }


        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0025,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0025);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0026,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0026);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x002A,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x002A);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 0;                                                    // Value sent to register                   
        WriteReg(0x002B,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x002B);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 0;                                                    // Value sent to register                   
        WriteReg(0x0004,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0004);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        //This is the register thats causing all the TEMP issues
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0059,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0059);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 0;                                                    // Value sent to register                   
        WriteReg(0x0047,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0047);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }
        //End of I generation TUNE
    } else {
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x001D,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x001D);                                       // Get returned value
        if (Commanded_value != Returned_value)
          {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        __delay_ms(1);
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0024,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0024);                                       // Get returned value
        if (Commanded_value != Returned_value)
          {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }


        __delay_ms(1);
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0025,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0025);                                       // Get returned value
        if (Commanded_value != Returned_value)
          {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }


        __delay_ms(1);
        Commanded_value = 7;                                                    // Value sent to register                   
        WriteReg(0x001E,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x001E);                                       // Get returned value
        if (Commanded_value != Returned_value)
          {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }


        __delay_ms(1);
        Commanded_value = 3;                                                    // Value sent to register                   
        WriteReg(0x001F,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x001F);                                       // Get returned value
        if (Commanded_value != Returned_value) { 
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
        }

        __delay_ms(1);
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0044,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0) {                                            // Write successful?
            Error_flag = 1;                                                     // Set error flag
             //goto Tune_Exit;                                                  // Exit if not
        }
        Returned_value = ReadReg(0x0044);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
        }
    } // END of H Generation code
     
    if(!CW) {
        //Both generations use reg 2 + 3 for tune

        __delay_ms(1);
        // Send Register 2 value
        Commanded_value = REG2_value;                                               // Value sent to register  
        WriteReg(0x0002,Commanded_value);                                           // Write value
        if (CheckStatus_flag == 0) {                                                // Write successful?
            Error_flag = 1;                                                         // Set error flag
            //goto Tune_Exit;                                                       // Exit if not
        }
        Returned_value = ReadReg(0x0002);                                           // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                         // Set error flag
            //goto Tune_Exit;                                                       // Exit
        }


        __delay_ms(1);
        // Send Register 3 value
        Commanded_value = REG3_value;                                               // Value sent to register  
        WriteReg(0x0003,Commanded_value);                                           // Write value
        if (CheckStatus_flag == 0) {                                                // Write successful?
            Error_flag = 1;                                                         // Set error flag
            //goto Tune_Exit;                                                       // Exit if not
        }
        Returned_value = ReadReg(0x0003);                                           // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                         // Set error flag
            //goto Tune_Exit;                                                       // Exit
        }
    }   
    
    __delay_ms(1);
    Tune_Exit:
    if (Error_flag == 0)                                                        // Everything OK?
    {
      WriteString("\r\nOK\r\n");
      ACK_LED();
    }

    if (Error_flag != 0)                                                        // Error detected?
    {
      if(CheckStatus_flag==0) { 
          WriteString("\r\nERR: CheckStatus_flag issue\r\n"); 
      } else {
          REG2_value = ReadReg(0x0002);
          REG3_value = ReadReg(0x0003);
          WriteString("\r\nERR: Register write failure\r\n");
          //Display_HEX16(REG2_value);
          //WriteString(" ");
          //Display_HEX16(REG3_value);
          //WriteString("\r\n");
      }
    }
    __delay_ms(2);
}

/*******************************************************************************
 *
 * Sets the registers for continues wave output and uses user values for 0x0->0x3
 * 
 ******************************************************************************/
int CALW() {

    cmdbuf_ptr = 5;                                                             // Set pointer to end of key word
    REG0_value = Find_HEX16_Number();                                           // Get register 0 value
    REG1_value = Find_HEX16_Number();                                           // Get register 1 value
    REG2_value = Find_HEX16_Number();                                           // Get register 2 value
    REG3_value = Find_HEX16_Number();                                           // Get register 3 value
    
    CheckStatus_flag = 1;
    icnt = 0;
    while(icnt<4000 && !CheckStatus_flag){CheckStatus_flag = CheckStatus();icnt+=1;}
    Returned_value = ReadReg(0x0044);      
    if(Returned_value == 0x1250) {                                              //Determine if the chip is I generation or H generation
        //WriteString("\r\nI Generation CHIP");
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0025,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0025);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            WriteString("\r\n1\r\n");
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0026,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0026);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            WriteString("\r\n2\r\n");
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x002A,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x002A);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            WriteString("\r\n3\r\n");
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x002B,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x002B);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            WriteString("\r\n4\r\n");
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0004,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0004);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            WriteString("\r\n5\r\n");
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0059,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0059);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            WriteString("\r\n6\r\n");
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        Commanded_value = 0;                                                    // Value sent to register                   
        WriteReg(0x0047,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0047);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            WriteString("\r\n7\r\n");
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }
        //End of I generation TUNE
    } else {
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x001D,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x001D);                                       // Get returned value
        if (Commanded_value != Returned_value)
          {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }

        __delay_ms(1);
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0024,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0024);                                       // Get returned value
        if (Commanded_value != Returned_value)
          {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }


        __delay_ms(1);
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0025,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x0025);                                       // Get returned value
        if (Commanded_value != Returned_value)
          {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }


        __delay_ms(1);
        Commanded_value = 7;                                                    // Value sent to register                   
        WriteReg(0x001E,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x001E);                                       // Get returned value
        if (Commanded_value != Returned_value)
          {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }


        __delay_ms(1);
        Commanded_value = 3;                                                    // Value sent to register                   
        WriteReg(0x001F,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0)                                              // Write successful?
        {
            CheckStatusError_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit if not
        }
        Returned_value = ReadReg(0x001F);                                       // Get returned value
        if (Commanded_value != Returned_value)
          { 
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }
        
        __delay_ms(1);
        Commanded_value = 1;                                                    // Value sent to register                   
        WriteReg(0x0044,Commanded_value);                                       // Write value
        if (CheckStatus_flag == 0) {                                            // Write successful?
            CheckStatusError_flag = 1;                                                     // Set error flag
             //goto Tune_Exit;                                                  // Exit if not
        }
        Returned_value = ReadReg(0x0044);                                       // Get returned value
        if (Commanded_value != Returned_value) {
            Error_flag = 1;                                                     // Set error flag
            //goto Tune_Exit;                                                   // Exit
          }
    }
    //Both generations use reg 0,1,2,3 for calw
    
    __delay_ms(1);
    // Send Register 0 value
    Commanded_value = REG0_value;                                               // Value sent to register  
    WriteReg(0x0000,Commanded_value);                                           // Write value
    if (CheckStatus_flag == 0) {                                                // Write successful?
        CheckStatusError_flag = 1;                                                         // Set error flag
        //goto Tune_Exit;                                                       // Exit if not
    }
    Returned_value = ReadReg(0x0000);                                           // Get returned value
    if (Commanded_value != Returned_value) {
        Error_flag = 1;                                                         // Set error flag
        //goto Tune_Exit;                                                       // Exit
    }

    __delay_ms(1);
    // Send Register 1 value
    Commanded_value = REG1_value;                                               // Value sent to register  
    WriteReg(0x0001,Commanded_value);                                           // Write value
    if (CheckStatus_flag == 0) {                                                // Write successful?
        CheckStatusError_flag = 1;                                                         // Set error flag
        //goto Tune_Exit;                                                       // Exit if not
    }
    Returned_value = ReadReg(0x0001);                                           // Get returned value
    if (Commanded_value != Returned_value) {
        Error_flag = 1;                                                         // Set error flag
        //goto Tune_Exit;                                                       // Exit
    }
    
    __delay_ms(1);
    // Send Register 2 value
    Commanded_value = REG2_value;                                               // Value sent to register  
    WriteReg(0x0002,Commanded_value);                                           // Write value
    if (CheckStatus_flag == 0) {                                                // Write successful?
        CheckStatusError_flag = 1;                                                         // Set error flag
        //goto Tune_Exit;                                                       // Exit if not
    }
    Returned_value = ReadReg(0x0002);                                           // Get returned value
    if (Commanded_value != Returned_value) {
        Error_flag = 1;                                                         // Set error flag
        //goto Tune_Exit;                                                       // Exit
    }

    
    __delay_ms(1);
    // Send Register 3 value
    Commanded_value = REG3_value;                                               // Value sent to register  
    WriteReg(0x0003,Commanded_value);                                           // Write value
    if (CheckStatus_flag == 0) {                                                // Write successful?
        CheckStatusError_flag = 1;                                                         // Set error flag
        //goto Tune_Exit;                                                       // Exit if not
    }
    Returned_value = ReadReg(0x0003);                                           // Get returned value
    if (Commanded_value != Returned_value) {
        Error_flag = 1;                                                         // Set error flag
        //goto Tune_Exit;                                                       // Exit
    }
    
    if (Error_flag == 0 && CheckStatusError_flag == 0) {                                                      // Everything OK?
      WriteString("\r\nOK\r\n");
      ACK_LED();
    }

    if (Error_flag != 0 || CheckStatusError_flag != 0)                                                        // Error detected?
    {
      if(CheckStatusError_flag == 1) { WriteString("\r\nERR: CheckStatus_flag issue\r\n"); 
      } else {
          WriteString("\r\nERR: Register write failure\r\n");
      }
    }
}

/*******************************************************************************
 *
 * Reads registers 0x0->0x3
 * 
 ******************************************************************************/
int CALR() {
    REG0_value = ReadReg(0x0000);
    REG1_value = ReadReg(0x0001);
    REG2_value = ReadReg(0x0002);
    REG3_value = ReadReg(0x0003);
    WriteString("\r\n0x00=");
    Display_HEX16(REG0_value);
    WriteString(" 0x01=");
    Display_HEX16(REG1_value);
    WriteString(" 0x02=");
    Display_HEX16(REG2_value);
    WriteString(" 0x03=");
    Display_HEX16(REG3_value);
    WriteString("\r\nOK\r\n");
}
// -----------------------------------------------------------------------------
//
// List Commands
//
// All possible commands are output to serial port
//
//------------------------------------------------------------------------------
void List_Commands()
{
   //WriteString("********** Proto-C **********\r\n\r\n");
   //__delay_ms(5);
    int R_Data = ReadFlash(SERIAL_ADDR);
    /* Read data back */                                                        // Get read-back WR data
    WriteString("\r\nSER: ");
    Display_HEX16(R_Data);                                                      // Display data
    WriteString(" ");
    __delay_ms(2); 
    R_Data = ReadFlash(CODEVER_ADDR);
    /* Read data back */                                                        // Get read-back WR data
    WriteString("VER: ");   
    Display_HEX16(R_Data);                                                      // Display data
    WriteString("\r\n");
    
   WriteString("Commands:\r\n");
   __delay_ms(5);

   
   WriteString("BATT       - Battery Voltage\r\n");
   __delay_ms(5);
   
   
   WriteString("CALW       - CALW=XX YY ZZ AA\r\n");
   __delay_ms(5);
   
   WriteString("CALR       - CALR\r\n");
   __delay_ms(5);
   
   
   WriteString("CONT       - Continuity Check\r\n");
   __delay_ms(5);
   
   WriteString("CW         - Enable CW mode\r\n");
   __delay_ms(5);
   
   
   WriteString("GEN        - Display Generation\r\n");
   __delay_ms(5);
   
   WriteString("IDRD       - Read Chip ID\r\n");
   __delay_ms(5);

    
   WriteString("IDWR       - IDWR=XXXX\r\n");
   __delay_ms(5);
   
   WriteString("IDWRS      - IDWR: Keeps registers set\r\n");
   __delay_ms(5);
   
   
   //WriteString("INIT       - Initializes ProtoC\r\n");
   //__delay_ms(5);
   
   
   WriteString("LED        - Toggles LED\r\n");
   __delay_ms(5);

    
   WriteString("READ       - READ=ABCD\r\n");
   __delay_ms(5);
   
   
   WriteString("RESET      - Resets ProtoC\r\n");
   __delay_ms(5);


   WriteString("TEMP       - Temperature value\r\n");
   __delay_ms(5);

    
   WriteString("TUNE       - TUNE=XX YY\r\n");
   __delay_ms(5);

   
   WriteString("VER        - Display Version\r\n");
   __delay_ms(5);
   
   WriteString("WRITE      - WRITE=ABCD 1234\r\n");
   __delay_ms(5);
   
   //WriteString("\r\n");
   //WriteString("DEBUG ?    - View Debug options\r\n");
   //__delay_ms(5);
   
   /*
   WriteString("FWRITE     - FWRITE=ABCDEF 1234\r\n");
   __delay_ms(5);
   
   
   WriteString("FREAD      - FREAD=ABCDEF\r\n");
   __delay_ms(5);
   
   
   WriteString("             ABCDEF=SERIAL to read/write serial number\r\n");
   __delay_ms(5);
   WriteString("             ABCDEF=CODVER to read/write code version\r\n");
   __delay_ms(5);
   WriteString("             1234 will create code version 12.34\r\n");
   __delay_ms(5);
   */
   WriteString("\r\n");
   WriteString("HELP       - Lists Commands\r\n");
   __delay_ms(5);


} // List_Commands

void List_Debug() {
  WriteString("DEBUG commands:\r\n");
  __delay_ms(5);
  WriteString("DEBUG EN    - Enable debug\r\n");
  __delay_ms(5);
  WriteString("ATBS=XX     - Set ATB select register\r\n");
  __delay_ms(5);
  WriteString("ATB Select values\r\n");
  __delay_ms(5);
  WriteString("     0      - ocs_bias\r\n");
  __delay_ms(5);
  WriteString("     1      - vss\r\n");
  __delay_ms(5);
  WriteString("     2       - VDDA2\r\n");
  __delay_ms(5);
  WriteString("     3       - VDDD\r\n");
  __delay_ms(5);
  WriteString("     4       - clk_50k\r\n");
  __delay_ms(5);
  WriteString("     5       - clk_rtc\r\n");
  __delay_ms(5);
  WriteString("     6       - bit_out\r\n");
  __delay_ms(5);
  WriteString("     7       - clk_hs\r\n");
  __delay_ms(5);
}

// *****************************************************************************
//
//  Main
//
// *****************************************************************************

void WriteString(const char *string);

int main(void) {
    int i;
    //OSCCON = 0x73;                                                            // Set 8 MHz Internal Oscillator
    //SSPCON1 = 0x21;                                                           //Enable SPI mode
    //SSPSTAT = 0xc0;                                                           //sample at end/transmit active to idle
    
    PORTA = 0;                                                                  // Initialize PortA
    PORTB = 0x80;                                                               // Initialize PortB
    PORTC = 0x40;                                                               // Initialize PortC (make sure RC6 is high)

    // Configure PortA bits
    // 1 = Input, 0 = Output
    TRISAbits.TRISA7 = 0;   // CLKOUT (Not Used)
    TRISAbits.TRISA6 = 0;   // CLKIN (Not Used)
    
    TRISAbits.TRISA5 = 1;   // AN4 - STUART_CS
    TRISAbits.TRISA3 = 1;   // AN3 - STUART_SCK
    TRISAbits.TRISA2 = 1;   // AN2 - STUART_MOSI
    TRISAbits.TRISA1 = 1;   // AN1 - STUART_MISO
    TRISAbits.TRISA0 = 1;   // AN0 - STUART_RESET
    
    
    // Configure PortB bits
    // 1 = Input, 0 = Output
    TRISBbits.TRISB7 = 1;   // PIC_DATA
    TRISBbits.TRISB6 = 1;   // PIC_CLK
    TRISBbits.TRISB5 = 0;   // NC
    TRISBbits.TRISB4 = 0;   // NC
    TRISBbits.TRISB3 = 0;   // TEST
    TRISBbits.TRISB2 = 1;   // AN8 - BATT_MON
    TRISBbits.TRISB1 = 0;   // LED2
    TRISBbits.TRISB0 = 0;   // LED1
    
    
    // Configure PortC bits
    // 1 = Input, 0 = Output
    TRISCbits.TRISC7 = 1;   // PIC_RX
    TRISCbits.TRISC6 = 0;   // PIC_TX
    TRISCbits.TRISC5 = 0;   // MOSI
    TRISCbits.TRISC4 = 1;   // MISO
    TRISCbits.TRISC3 = 0;   // SCLK
    TRISCbits.TRISC2 = 0;   // MOD_CS
    TRISCbits.TRISC1 = 0;   // NC
    TRISCbits.TRISC0 = 0;   // MOD_RES



    // Initialize COM0 Rings
    Initialize_COM0_RX();                                                       // Initialize COM0 RX ring
    Initialize_COM0_TX();                                                       // Initialize COM0 TX ring

    //
    // Configure A/D
    //
    // Disable rest of Analog Inputs pins
    // so that there is no conflict with I/O pins
    //
    //ANSEL = 0x04;                                                               // Select AN2
    //ANSELH = 0x00;
    
    //CONFIG3H = 0xFE                                                           // 12-bit A/D
    ANCON0 = 0xE0;                                                              // AN0 ~ AN4 enabled
    ANCON1 = 0x1F;                                                              // Disable remaining ANn pins
    
    ADCON1 = 0xA3;                                                              // Right Justify Data / CLK = A/D osc
    ADCON0 = 0x01;                                                              // Enable ADC / Set AN0 to MUX / Ref = VDD
    
    MUX_chnl = 0;                                                               // Set MUX to first AN0
    ADCON0 = 0x03;                                                              // MUX = 0 / ADC = ON / Start Conversion
    
    //
    // Configure UART
    //
    //ANSELHbits.ANSEL11 = 0;                                                   // Disable AN11 for RX pin (RB5)

    RCSTA1bits.SPEN = 1;                                                        // Enable Serial Port
    RCSTA1bits.RX9 = 0;                                                         // 8 Data bits
    RCSTA1bits.CREN = 1;                                                        // Enable Continuous Receive
    RCSTA1bits.ADDEN = 0;                                                       // Disable Address Detection

    
    TXSTA1bits.TX9 = 0;                                                         // Set 8 Data Bits
    TXSTA1bits.SYNC = 0;                                                        // Asynchronous Mode
    TXSTA1bits.TXEN = 1;                                                        // En TX

    // Set Baud Rate
    // Set Baud Rate to 8-bit Asynchronous Mode
    // Baud Rate = Fosc / 16 (n + 1)
    TXSTA1bits.BRGH = 0;                                                        // Set BRGH = 0
    BAUDCON1bits.BRG16 = 1;                                                     // Set BRG16 = 1

    // 9600 Baud --> "n" = 162 for 25MHz crystal
    // 9600 Baud --> "n" = 104 for 16MHz
    // 9600 Baud --> "n" = 51 for 8MHz
    // 19200 Baud--> "n" = 52 for 16 MHz
    // 115200 Baud--> "n" = 25 for 48 MHz
    // Set Baud Rate Registers
    SPBRGH1 = 0x00;
    SPBRG1 = 25;
    
    // Enable RX Interrupts
    PIE1bits.RCIE = 1;                                                          // Enable RX Interrupts
    COM0_data = RCREG;                                                          // Dummy read to clear out RX register and RCIF bit

    // Enable Interrupt Priority Levels
    RCONbits.IPEN = 1;                                                          // Enable Interrupt Priority Levels

    // Enable High Priority Interrupts
    IPR1bits.TXIP = 1;                                                          // Enable High Priority for TX Interrupts
    IPR1bits.RCIP = 1;                                                          // Enable High Priority for RX Interrupts

    // Global Interrupts
    INTCONbits.GIEL = 0;                                                        // Disable Low-Priority Interrutps
    INTCONbits.GIEH = 1;                                                        // Enable High-Priority Interrupts

    ToggleIt = 0;                                                               // Initialize LED toggle variable
    LED_Flash_Timer = 0;                                                        // Reset LED timer
    index = 0;                                                                  // Reset CMD Buffer index pointer;
    

    //Set DEBUG to off
    FlashErase(DEBUG_ADDR);
    WriteFlash(DEBUG_ADDR,0x0000);
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Startup Message
//
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    WriteString("\r\n\r\n\r\n\r\n");
    __delay_ms(10);

    WriteString("********** Proto-C Contactor **********\r\n\r\n");
    __delay_ms(10);


    List_Commands();                                                            // Output Commands
    WriteString("\r\n>");

// -----------------------------------------------------------------------------
//
// Configure ProtoC Module
//
//------------------------------------------------------------------------------    
    //Config();
    CS = 1;                                                                     // Set Chip Select line high
    SDO = 0;                                                                    // Set SDO low
    SCLK = 0;                                                                   // Set SCLK low
            


// -----------------------------------------------------------------------------
//
// Sequence LEDs
//
//------------------------------------------------------------------------------    
    
   LED1 = 1;                                                                    // Turn LED on
   LED_delay();
   LED1 = 0;                                                                    // Turn LED off

   LED2 = 1;                                                                    // Turn LED on
   LED_delay();
   LED2 = 0; 



// -----------------------------------------------------------------------------
//
// Main Loop
//
//------------------------------------------------------------------------------

    while (1)
    {

        //......................................................................
        //
        // Parse Commands
        //
        //......................................................................
        if ( Get_CMD_String() )
        {
            
            //------------------------------------------------------------------
            // Version Number
            //------------------------------------------------------------------
            if (stricmp(CMD_buffer, "VER") == 0)                                // Match CMD string?
            {
                int R_Data = ReadFlash(SERIAL_ADDR);
                /* Read data back */                                            // Get read-back WR data
                WriteString("\r\nSER: ");
                Display_HEX16(R_Data);                                          // Display data
                WriteString(" ");
                __delay_ms(2); 
                R_Data = ReadFlash(CODEVER_ADDR);
                /* Read data back */                                            // Get read-back WR data
                WriteString("VER: ");
                Display_HEX16(R_Data);                                          // Display data
                WriteString("\r\n");
                __delay_ms(2);
                ACK_LED();                                                      // Flash LED to ACK command
            } // RESET

            //------------------------------------------------------------------
            // Chip Generation
            //------------------------------------------------------------------
            if(stricmp(CMD_buffer, "GEN") == 0) {
                Returned_value = ReadReg(0x0044);      
                if(Returned_value == 0x1250) {                                  //Determine if the chip is I generation or H generation
                    WriteString("\r\nGeneration I Controller\r\n");
                } else if (CheckStatus_flag != 0) {
                    WriteString("\r\nGeneration H Controller\r\n");
                } else {
                    WriteString("\r\nNo controller connected\r\n");
                }
            }
            
            //------------------------------------------------------------------
            // Toggle LED
            //------------------------------------------------------------------
            if (stricmp(CMD_buffer, "LED") == 0)                                // Match CMD string?
            {
                LED2 = LED2 ^ 1;                                                // Toggle LED

                WriteString("\r\nOK\r\n");
                //ACK_LED();                                                    // Flash LED to ACK command
            } // LED
            

            //------------------------------------------------------------------
            // RESET command
            //------------------------------------------------------------------
            if (stricmp(CMD_buffer, "RESET") == 0 || stricmp(CMD_buffer, "res")==0)                              // Match CMD string?
            {
                RES = 1;                                                        // Set RESET line high
                __delay_ms(1);                                                  // Hang out a little...
                RES = 0;                                                        // Return RESET line to inactive state
                __delay_ms(1);
                
                WriteString("\r\nOK\r\n");
                ACK_LED();                                                      // Flash LED to ACK command
            } // RESET
            
            
            //------------------------------------------------------------------
            // INIT command
            //------------------------------------------------------------------
            if (stricmp(CMD_buffer, "INIT") == 0)                               // Match CMD string?
            {
                Config();
                                
                WriteString("\r\nOK\r\n");
                ACK_LED();                                                      // Flash LED to ACK command
            } // INIT
            

            //------------------------------------------------------------------
            // CONFIG command
            //------------------------------------------------------------------
            if (stricmp(CMD_buffer, "CONFIG") == 0)                             // Match CMD string?
            {
                Config();                                                       // Configure PLL
                WriteString("\r\nOK\r\n");
                ACK_LED();                                                      // Flash LED to ACK command
            } // CONFIG
            
            
            //------------------------------------------------------------------
            // CONTINUITY command
            //------------------------------------------------------------------
            if (stricmp(CMD_buffer, "CONT") == 0)                               // Match CMD string?
            {
                unsigned int status, NOM_voltage, Tolerance;
                unsigned int Error_Flag;
                
                Error_Flag = 0;                                                 // Reset error flag
                
                //Read_AN0_AN4();                                                 // Read analog inputs
                
                // Test Code
                // Display analog inputs prior to test mode
//                WriteString("\r\nAN0 = ");
//                Display_HEX16(AN0_data);                                        // Display data
//                __delay_ms(5);
//                WriteString("\r\nAN1 = ");
//                Display_HEX16(AN1_data);                                        // Display data
//                __delay_ms(5);
//                WriteString("\r\nAN2 = ");
//                Display_HEX16(AN2_data);                                        // Display data
//                __delay_ms(5);
//                WriteString("\r\nAN3 = ");
//                Display_HEX16(AN3_data);                                        // Display data
//                __delay_ms(5);
//                WriteString("\r\nAN4 = ");
//                Display_HEX16(AN4_data);                                        // Display data
//                __delay_ms(5);
                     
                
                // Throuw the Switch !!!!
                // Raise the pullup resistor voltage to > the battery voltage
                TEST = 1;
                __delay_ms(10);                                                 // Wait for the dust to settle...
                
                Read_AN0_AN4();                                                 // Read analog inputs
                
                // Test Code
                // Display analog inputs in test mode
//                WriteString("\r\nAN0 = ");
//                Display_HEX16(AN0_data);                                        // Display data
//                __delay_ms(5);
//                WriteString("\r\nAN1 = ");
//                Display_HEX16(AN1_data);                                        // Display data
//                __delay_ms(5);
//                WriteString("\r\nAN2 = ");
//                Display_HEX16(AN2_data);                                        // Display data
//                __delay_ms(5);
//                WriteString("\r\nAN3 = ");
//                Display_HEX16(AN3_data);                                        // Display data
//                __delay_ms(5);
//                WriteString("\r\nAN4 = ");
//                Display_HEX16(AN4_data);                                        // Display data
//                __delay_ms(5);
                
                // Turn OFF Test Switch
                TEST = 0;                   
                
                
                //  Nominal voltage = Battery Voltage, Tolerance = 0.5V
                Read_AN8();                                                     // Read analog input (Battery Voltage)
                NOM_voltage = (1.5/3.3) * 4095;                                 // Set minimum voltage for testing
                if (AN8_data < NOM_voltage) { Error_Flag = 3; }                 // Set error flag if not
                
                NOM_voltage = AN8_data;                                         // Nominal voltage is the battery voltage
                //NOM_voltage = (1.5/3.3) * 4095;                               // Calculate Nominal voltage

                
                
                // AN1 is an output and has a different protection structure - usually 0.4V
                Tolerance = (0.6/3.3) * 4095;                                   // Calculate Tolerance
                status = Check_Limits (AN1_data, NOM_voltage, Tolerance);       // Is ADC voltage within limits?
                if (status != 0) { Error_Flag = 1; }                            // Set error flag if not
                
                // AN0, AN2 ~ AN4 have the same protection diodes - usually 0.7V
                Tolerance = (0.9/3.3) * 4095;                                   // Calculate Tolerance
                status = Check_Limits (AN0_data, NOM_voltage, Tolerance);       // Is ADC voltage within limits?
                if (status != 0) { Error_Flag = 1; }                            // Set error flag if not
                
                status = Check_Limits (AN2_data, NOM_voltage, Tolerance);       // Is ADC voltage within limits?
                if (status != 0) { Error_Flag = 1; }                            // Set error flag if not
                
                status = Check_Limits (AN3_data, NOM_voltage, Tolerance);       // Is ADC voltage within limits?
                if (status != 0) { Error_Flag = 1; }                            // Set error flag if not
                
                status = Check_Limits (AN4_data, NOM_voltage, Tolerance);       // Is ADC voltage within limits?
                if (status != 0) { Error_Flag = 1; }                            // Set error flag if not
                
                
                // Check Error Flag and finish up
                if (Error_Flag == 0)
                {
                    WriteString("\r\nOK\r\n");
                    __delay_ms(2);
                    ACK_LED();
                } else if (Error_Flag == 3) {
                    WriteString("\r\nERR: DUT VOLTAGE\r\n");
                    __delay_ms(2);
                    ACK_LED();
                } else {
                    WriteString("\r\nERR: CONT\r\n");
                    __delay_ms(2);
                    ACK_LED();
                }
                
            } // CONT
            
            
            //------------------------------------------------------------------
            // Battery Monitor command
            //------------------------------------------------------------------
            if (stricmp(CMD_buffer, "BATT") == 0)                               // Match CMD string?
            {
                unsigned long temp;
                
                Read_AN8();                                                     // Read analog input
                
                WriteString("\r\nBATT = ");
                
                temp = AN8_data;                                                // Convert to unsigned long
                temp = temp * 3300;                                             // Scale value to mV
                temp = temp / 4095;
                                
                AN8_data = temp;                                                // Save scaled value
                
                Display_DEC(AN8_data);                                          // Display data
                WriteString(" mV");                                             // Display units
                __delay_ms(5);
                
                WriteString("\r\nOK\r\n");
                __delay_ms(2);
                ACK_LED();
                
            } // BATT
            
            //------------------------------------------------------------------
            // READ data command
            //------------------------------------------------------------------
            if ( (toupper(CMD_buffer[0]) == 'R'))                               // Match CMD string?
            {
                if (toupper(CMD_buffer[3]) == 'D')                              // ReaD command?
                {
                    if (CMD_buffer[4] == '=')                                   // "=" ?
                    {
                    cmdbuf_ptr = 5;                                             // Set pointer to end of key word
                    Read_Address = Find_HEX16_Number();
                    Read_Data = ReadReg(Read_Address);                          // Read data from ProtoC
                    WriteString("\r\n");
                    Display_HEX16(Read_Address);                                // Display address
                    WriteString(" = ");
                    Display_HEX16(Read_Data);                                   // Display data
                    WriteString("\r\n");
                   __delay_ms(2);
                        
                    WriteString("OK\r\n");
                    ACK_LED();
                  __delay_ms(2);
                   
                    } // If (CMD_buffer[4]...)
                } // If (CMD_buffer[3]...)
            } // READ
            
            
            //------------------------------------------------------------------
            // Write data command
            //------------------------------------------------------------------
            if ( (toupper(CMD_buffer[0]) == 'W'))                               // Match CMD string?
            {
                if (toupper(CMD_buffer[1]) == 'R')                              // WRite command?
                {
                    if (CMD_buffer[5] == '=')                                   // "=" ?
                      {
                      cmdbuf_ptr = 6;                                           // Set pointer to end of key word
                      WR_Address = Find_HEX16_Number();                         // Get address 
                      WR_Data = Find_HEX16_Number();
                      CheckStatus_flag = WriteReg(WR_Address,WR_Data);
                      if(CheckStatus_flag == 1) {
                        /* Read data back */
                        Read_Data = ReadReg(WR_Address);                        // Get read-back WR data
                        WriteString("\r\n");
                        Display_HEX16(WR_Address);                              // Display address
                        WriteString(" = ");
                        Display_HEX16(Read_Data);                               // Display data
                        WriteString("\r\n");
                        if(WR_Data == Read_Data) {
                            __delay_ms(2);            
                            WriteString("OK\r\n");
                            ACK_LED();
                            __delay_ms(2);
                        } else {
                            __delay_ms(2);
                            WriteString("ERR\r\n");
                            ACK_LED();
                            __delay_ms(2);
                        }
                      } else {
                          WriteString("\r\nERR:CheckStatus_flag issue\r\n");
                      }
                      } // If (CMD_buffer[4]...)
                } // If (CMD_buffer[3]...)
            } // WRITE
            
            
            //------------------------------------------------------------------
            // Chip ID Read command
            //------------------------------------------------------------------
            if (toupper(CMD_buffer[0]) == 'I')                                  // Match CMD string?
            {
                if (toupper(CMD_buffer[1]) == 'D')                              // ID command?
                {
                    if (toupper(CMD_buffer[2]) == 'R')                          // Start of Read command?
                    {
                        if (toupper(CMD_buffer[3]) == 'D')                      // "ReaD command ?
                          {
                            IDRD();
                          } // If (CMD_buffer[3]...)
                    } // if (CMD_buffer[2])
                } // If (CMD_buffer[1]...)
            } // Chip ID Read
            
            
            //------------------------------------------------------------------
            // Chip ID Write command
            //------------------------------------------------------------------
            if (toupper(CMD_buffer[0]) == 'I')                                  // Match CMD string?
            {
                if (toupper(CMD_buffer[1]) == 'D')                              // ID command?
                {
                    if (toupper(CMD_buffer[2]) == 'W')                          // Write command?
                    {
                        if (CMD_buffer[4] == '=')                               // "=" ?
                          {
                            IDWR(0);
                          } // If (CMD_buffer[4]...) {
                        else if (toupper(CMD_buffer[4])== 'S') {
                            IDWR(1);
                        }
                    } // if (CMD_buffer[2])
                } // If (CMD_buffer[1]...)
            } // Chip ID Write
            
            
            //------------------------------------------------------------------
            // TEMP command
            //------------------------------------------------------------------
            if (toupper(CMD_buffer[0]) == 'T')                                  // Match CMD string?
            {
                if (toupper(CMD_buffer[1]) == 'E')                              // ID command?
                {
                    if (toupper(CMD_buffer[2]) == 'M')                          // Start of Temp command?
                    {
                        if (toupper(CMD_buffer[3]) == 'P')                      // "TEMP command ?
                          {
                          if (toupper(CMD_buffer[4]) == '=') {
                              cmdbuf_ptr = 5;
                              WR_Data = Find_HEX16_Number();
                              //WriteString("\r\n");
                              //Display_HEX16(WR_Data);
                          } else {
                              WR_Data = 0xFFFF;
                          }
                          int check_status=0;
                          int zeros=0;
                          int ffffs=0;
                          int ids=0;
                          lcv=0;
                          TEMP_data = 0;                                        // Initialize Temp data value for testing
                          do {
                            Returned_value = ReadReg(0x0044); 
                            if(Returned_value == 0x1250) {                      //Determine if the chip is I generation or H generation
                                WriteReg(0x3D,0xFF);                            //If I gen, configure temp
                                WriteReg(0x3E,1);
                                WriteReg(0x3F,1);
                                WriteReg(0x40,0x1F);
                                if(WR_Data != 0xFFFF) {
                                    WriteReg(0x41,WR_Data);
                                }
                                WriteReg(0x42,2);
                                WriteReg(0x5A,1);
                                GEN_REG = 0x73;
                            } else {
                                GEN_REG = 0x4A;
                            }
                            WriteReg(GEN_REG,0xFFFF);                           // Trigger temp conversion
                            __delay_ms(15);
                            __delay_ms(15);
                            __delay_ms(15);
                            __delay_ms(15);
                            while(i<1000 & !CheckStatus_flag){CheckStatus_flag = CheckStatus();i+=1;}
                            if(!CheckStatus_flag) { check_status++; lcv++; continue;}
                            if(Returned_value == 0x1250) {                      //Determine if the chip is I generation or H generation
                                GEN_REG = 0x38;
                            } else {
                                GEN_REG = 0x37;
                            }
                            TEMP_data = ReadReg(GEN_REG);                       // Read data from ProtoC
                            __delay_ms(5);
                            lcv++;
                            if(TEMP_data == 0xFFFF) { ffffs++; }
                            if(TEMP_data == 0x0000) { zeros++; }
                            if(TEMP_data == 0x1250) { ids++;   }
                          } while((TEMP_data == 0xFFFF || TEMP_data == 0x1250) && lcv < 25);
                          if(lcv==25) {
                              if(ReadFlash(DEBUG_ADDR) == 1) {
                                if (check_status>1) { WriteString("\r\nCheck_status error: *"); Display_HEX16(check_status); __delay_ms(5);}
                                if (ffffs>1) { WriteString("\r\n0xFFFF error: *"); Display_HEX16(ffffs); __delay_ms(5);}
                                if (zeros>1) { WriteString("\r\n0x0000 error: *"); Display_HEX16(zeros); __delay_ms(5);}
                                if (ids>1)   { WriteString("\r\n0x1250 error: *"); Display_HEX16(ids);   __delay_ms(5);}
                              }
                              WriteString("\r\nTEMP ERROR\r\n"); goto temp_end; 
                          } else {
                            WriteString("\r\nTEMP= ");
                            Display_HEX16(TEMP_data);                           // Display data
                            WriteString("\r\n");
                            __delay_ms(2);
                            ACK_LED();  
                          }
                        } // If (CMD_buffer[3]...)
                    } // if (CMD_buffer[2])
                } // If (CMD_buffer[1]...)
            } // Chip ID Read
            temp_end:
            
            //------------------------------------------------------------------
            // Tune command
            //------------------------------------------------------------------
            if ( (toupper(CMD_buffer[0]) == 'T'))                               // Match CMD string?
            {
                if (toupper(CMD_buffer[1]) == 'U')                              //TUne command?
                {
                    if (CMD_buffer[4] == '=') {                                  // "=" ?
                      TUNE(0);
                    }
                }
            } // TUNE
            
            //------------------------------------------------------------------
            // CW command enabled continuous wave mode without changing 2+3
            //------------------------------------------------------------------
            if (stricmp(CMD_buffer, "CW") == 0) {
                TUNE(1);
            }
            //------------------------------------------------------------------
            // Calibration commands
            //------------------------------------------------------------------
            if ( (toupper(CMD_buffer[0]) == 'C'))                               // Match CMD string?
            {
                if (toupper(CMD_buffer[1]) == 'A') {                              //CAlW/R command?
                    if ( (toupper(CMD_buffer[3]) == 'W')) {    
                        CALW();
                    } else if (toupper(CMD_buffer[3]) == 'R') {
                        CALR();  
                    }
                }
            }
            
            //------------------------------------------------------------------
            
            // --------------------- Debug Options -----------------------------
            if(stricmp(CMD_buffer, "DEBUG ?") == 0) {
                WriteString("\r\n\r\n");
                List_Debug();
                
            }
            
            if(stricmp(CMD_buffer, "DEBUG EN") == 0) {
                Returned_value = ReadReg(0x0044);      
                if(Returned_value == 0x1250) {                                  //Determine if the chip is I generation or H generation
                    GEN_REG = 0x70;
                } else {
                    GEN_REG = 0x4D;
                }
                
                WriteReg(GEN_REG, 1);
                if(ReadReg(GEN_REG) == 1) {
                    WriteString("\r\nOK\r\n");
                } else {
                    WriteString("\r\nERR\r\n");
                }
            }
            
            if(toupper(CMD_buffer[0]) == 'A' && toupper(CMD_buffer[2]) == 'B' && toupper(CMD_buffer[3])=='S') {
                cmdbuf_ptr = 5;                                                 // Set pointer to end of key word
                WR_Data = Find_HEX16_Number();                                  // Save Chip ID to write
                
                Returned_value = ReadReg(0x0044);      
                if(Returned_value == 0x1250) {                                  //Determine if the chip is I generation or H generation
                    GEN_REG = 0x71;
                } else {
                    GEN_REG = 0x4E;
                }
                
                WriteReg(GEN_REG,WR_Data);
                if(ReadReg(GEN_REG) == WR_Data) {
                    WriteString("\r\nOK\r\n");
                } else {
                    WriteString("\r\nERR ATB = ");
                    Display_HEX16(ReadReg(GEN_REG));
                    WriteString("\r\n");
                }
            }
            
             //------------------------------------------------------------------
            // Flash Read
            //------------------------------------------------------------------
            if ( (toupper(CMD_buffer[0]) == 'F')) {
                if ( (toupper(CMD_buffer[1]) == 'R')) {
                    if ( CMD_buffer[5] == '=') {                                //We have a flash write command
                        //Check if its any mem address or a specific address
                        if( (toupper(CMD_buffer[6])) == 'S') {
                            if ((toupper(CMD_buffer[7])) == 'E') {             //We have a SERIAL read
                                FWR_Address = SERIAL_ADDR;                      // Set address 
                            }
                        } else if(toupper(CMD_buffer[6])== 'C') {
                            if(toupper(CMD_buffer[7]) == 'O') {
                                FWR_Address = CODEVER_ADDR;                     //Set address
                            }
                        } else {
                            cmdbuf_ptr = 6;                                    //set pointer to end of key-word
                            FWR_Address = Find_HEX24_Number();                  //get address
                        }
                        if(FWR_Address>=0x010000){
                            WriteString("\r\nERROR: Mem out of range\r\n");
                        } else {
                            int R_Data = ReadFlash(FWR_Address);
                            /* Read data back */                                // Get read-back WR data
                            WriteString("\r\n");
                            Display_HEX24(FWR_Address);                         // Display address
                            WriteString(" = ");
                            Display_HEX16(R_Data);                              // Display data
                            WriteString("\r\n");
                            __delay_ms(2);   
                            WriteString("\r\nOK\r\n");
                            ACK_LED();
                           __delay_ms(2);
                        }
                    }
                    
                }
            }
            
            //------------------------------------------------------------------
            // Flash Write
            //------------------------------------------------------------------
            
            if ( (toupper(CMD_buffer[0]) == 'F')) {
                if ( (toupper(CMD_buffer[1]) == 'W')) {
                    if ( CMD_buffer[6] == '=') {
                        //Check if its any mem address or a specific address
                        if( (toupper(CMD_buffer[7])) == 'S') {
                            if ((toupper(CMD_buffer[8])) == 'E') {             //We have a SERIAL read
                                FWR_Address = SERIAL_ADDR;
                            }
                        } else if(toupper(CMD_buffer[7])== 'C') {
                            if(toupper(CMD_buffer[8]) == 'O') {
                                FWR_Address = CODEVER_ADDR;                     //Set address
                            }
                        } else if (toupper(CMD_buffer[7]) == 'D') {
                            if (toupper(CMD_buffer[8]) == 'E') {
                                FWR_Address = DEBUG_ADDR;
                            }
                        } else if (toupper(CMD_buffer[7])=='B') {
                            if(toupper(CMD_buffer[8]) == 'O') {
                                cmdbuf_ptr = 9;
                                FWR_Address = SERIAL_ADDR;
                                WR_Data = Find_HEX16_Number();
                                FlashErase(FWR_Address);
                                WriteFlash(FWR_Address,WR_Data);
                                FWR_Address = CODEVER_ADDR;
                                WR_Data = Find_HEX16_Number();
                                FlashErase(FWR_Address);
                                WriteFlash(FWR_Address,WR_Data);

                                int R_Data = ReadFlash(SERIAL_ADDR);
                                /* Read data back */                            // Get read-back WR data
                                WriteString("\r\nSER: ");
                                Display_HEX16(R_Data);                          // Display data
                                WriteString(" ");
                                __delay_ms(2); 
                                R_Data = ReadFlash(CODEVER_ADDR);
                                /* Read data back */                            // Get read-back WR data
                                WriteString("VER: ");
                                Display_HEX16(R_Data);                          // Display data
                                WriteString("\r\n");
                                __delay_ms(2);   
                                WriteString("\r\nOK\r\n");
                                ACK_LED();              
                                goto fwrite_end;
                            }
                        } else {
                            WriteString("\r\nERR:Invalid memory address\r\n");
                            goto fwrite_end;
                        }
                        cmdbuf_ptr = 9;
                        FlashErase(FWR_Address);
                        WR_Data = Find_HEX16_Number();
                        WriteFlash(FWR_Address,WR_Data);
                        /* Read data back */
                        WR_Data = ReadFlash(FWR_Address);                    // Get read-back WR data
                        WriteString("\r\n");
                        Display_HEX24(FWR_Address);                          // Display address
                        WriteString(" = ");
                        Display_HEX16(WR_Data);                             // Display data
                        WriteString("\r\n");
                       __delay_ms(2);            
                        WriteString("\r\nOK\r\n");
                        ACK_LED();
                       __delay_ms(2);
                    }
                }
            }
            fwrite_end:
            
            //------------------------------------------------------------------

            if (stricmp(CMD_buffer, "Sleep") == 0)                              // Match CMD string?
            {
                WriteString("\r\nOK\r\n");
                __delay_ms(2);
                ACK_LED();                                                      // Flash LED to ACK command
                LATCbits.LATC6 = 0;                                             // Set RC6 low to reset USB chip
                Sleep();                                                        // Put processor to sleep
            }

            //------------------------------------------------------------------

            if (stricmp(CMD_buffer, "Help") == 0)                               // Match CMD string?
            {
                List_Commands();                                                // Show Commands
            }

            //------------------------------------------------------------------

            if (stricmp(CMD_buffer, "?") == 0)                                  // Match CMD string?
            {
                List_Commands();                                                // Show Commands
            }

            //------------------------------------------------------------------
            //
            //  End of Commands
            //  Flush out CMD_buffer
            //
            //------------------------------------------------------------------
            
            //unsigned int i;
            //for (i=0; i<rx0_buf_siz; i++)
            //{
            //    CMD_buffer[i] = 0;                                              // Erase CMD_buffer
           //}

            
            index = 0;                                                          // Reset index pointer
            WriteString("\r\n>");
        } // if (Get_CMD_String())


        //......................................................................
        //
        // Toggle LED1
        //
        //......................................................................
        LED_Flash_Timer++;                                                      // Inc LED Flash timer
        if (LED_Flash_Timer == 100)                                              // Time to toggle LED?
        {
            LED_Flash_Timer = 0;                                                // Reset LED toggle timer
            ToggleIt = ToggleIt ^ 0x01;                                         // Toggle LED bit
            LATBbits.LATB0 = ToggleIt;                                          // Output to LED1
        }
        
        
        //......................................................................
        //
        // Read ADC Data
        // This section will free-run and store conversion results in ADC_data[]]
        //
        //......................................................................
//        ADC_raw = ADRESH << 8;                                                  // Read MSB data and shift to upper byte
//        ADC_raw = ADC_raw | ADRESL;                                             // OR'in LSB data
//        ADC_data[MUX_chnl] = ADC_raw;                                           // Store conversion in ADC array
//        
//        MUX_chnl++;                                                             // Inc to next MUX channel
//        if (MUX_chnl > 4) MUX_chnl = 0;                                         // Wrap back to channel 0
//        
//        ADC_adcon0 = MUX_chnl << 2;                                             // Move MUX channel to correct position
//        ADC_adcon0 = ADC_adcon0 | 0x03;                                         // Set Start Conversion & ADC ON bits
//        
//        ADCON0 = ADC_adcon0;                                                    // Start next conversion
        
        //......................................................................
        //
        // Run Main Loop every 5ms
        //
        //......................................................................
        __delay_ms(5);

    } // while

  return;
} // main
